import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = () => (
  <Layout>
    <SEO title="Page two" />
    <h1>Hey esta es la segunda pagina</h1>
    <p>bienvenido a la pagina  2</p>
    <p>ala espera de un buen template de cv</p>
    <Link to="/">esto te regresa al comienzo</Link>
  </Layout>
)

export default SecondPage
