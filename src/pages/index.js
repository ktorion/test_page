import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>hola people :)</h1>
    <p>bienvenido al el sitio del gran gatsby en español.</p>
    <p>pensamos construir algo bueno desde aquí</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">ve a la pagina 2</Link>
  </Layout>
)

export default IndexPage
